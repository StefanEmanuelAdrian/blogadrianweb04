<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Post;
use App\Entity\User;
use App\Form\ArticleType;
use App\Form\CategoryType;
use App\Form\Comment1Type;
use App\Form\User1Type;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends AbstractController
{
    /**
     * @Route("/adminPanel", name="admin")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $posts= $em->getRepository(Post::class)->findAll();
        $categories = $em->getRepository(Category::class)->findAll();
        $users=$em->getRepository(User::class)->findAll();

        return $this->render('admin/adminPanel.html.twig', ['posts' => $posts, 'categories' => $categories,'users'=>$users]);
    }

    /**
 * @Route("/{id}/edit", name="category_edit", methods={"GET","POST"})
 */
    public function edit(Request $request, Category $category): Response
    {
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('category_index');
        }

        return $this->render('category/edit.html.twig', [
            'category' => $category,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="category_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Category $category): Response
    {
        if ($this->isCsrfTokenValid('delete'.$category->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($category);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin');
    }
    /**
     * @Route("/article/{id}/{name}", name="post-action")
     */
    public function post($id, $name)
    {
        try {
            $entityManager = $this->getDoctrine()->getManager();
            $post = $entityManager->getRepository(Post::class)->find($id);

            if (is_null($post)) {
                throw new \Exception("A post with id:$id does not exist!");
            }
        } catch (\Exception $ex){
            return $this->render('default/error.html.twig', ['errorMessage' => $ex->getMessage()]);
        }

        return $this->render('default/post.html.twig', ['post' => $post]);
    }
    /**
     * @Route("/article-add", name="article-add-action")
     */
    public function createPost(Request $request)
    {
        $article = new Post();
        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $article = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();
        }

        return $this->render('admin/addArticle.html.twig', ['form'=>$form->createView()]);
    }

    /**
     * @Route("/article-edit/{id}", name="article-edit-action")
     */
    public function editArticle(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $article = $entityManager->getRepository(Post::class)->find($id);
        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $article = $form->getData();

            $entityManager->persist($article);
            $entityManager->flush();
        }

        return $this->render('admin/addArticle.html.twig', ['form' => $form->createView()]);
    }
    /**
     * @Route("/article/{id}", name="article-delete-action", methods={"DELETE"})
     */
    public function deleteArticle(Request $request, Post $article,$id): Response
    {   $entityManager = $this->getDoctrine()->getManager();
        $article=$entityManager->getRepository(Post::class)->find($id);
        if ($this->isCsrfTokenValid('delete'.$article->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($article);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin');
    }

    /**
     * @Route("user/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function editUser(Request $request, User $user): Response
    {
        $form = $this->createForm(User::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     */
    public function deleteUser(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('admin');
    }
    /**
     * @Route("user/new", name="user_new", methods={"GET","POST"})
     */
    public function newUser(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(User1Type::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('admin');
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/addComment", name="add-comment", methods={"GET","POST"})
     */
    public function newComment(Request $request): Response
    {
        $comment = new Comment();
        $form = $this->createForm(Comment1Type::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($comment);
            $entityManager->flush();

            return $this->redirectToRoute('article-action');
        }

        return $this->render('admin/addComment.html.twig', [
            'comment' => $comment,
            'form' => $form->createView(),
        ]);
    }


}
