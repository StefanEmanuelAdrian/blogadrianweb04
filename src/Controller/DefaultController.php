<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Post;
use App\Entity\Subscriber;
use App\Form\ArticleType;
use App\Form\Comment1Type;
use App\Form\SubscriberType;
use App\Repository\PostRepository;
use Doctrine\ORM\Cache\Region;
use Doctrine\ORM\QueryBuilder;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;
use function Sodium\add;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $posts = $em->getRepository(Post::class)->findBy([], ['id' => 'DESC']);
        $categories = $em->getRepository(Category::class)->findAll();

        return $this->render('default/index.html.twig', ['posts' => $posts, 'categories' => $categories]);
    }

    /**
     * @Route("/blog", name="blogpage")
     */
    public function blog(Request $request, PaginatorInterface $paginator)
    {
        $em = $this->getDoctrine()->getManager();
         $posts = $em->getRepository(Post::class)->findBy([], ['id' => 'DESC']);
        $categories = $em->getRepository(Category::class)->findAll();
        $queryBuilder = $em->createQueryBuilder();
        $queryBuilder->select('p')
            ->from(Post::class, 'p');
        $pagination = $paginator->paginate(
            $queryBuilder, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            5/*limit per page*/
        );

        return $this->render('default/blog.html.twig', ['posts' => $posts, 'categories' => $categories, 'pagination' => $pagination]);
    }

    /**
     * @Route("article/{id}", name="article-action")
     */
    public function article($id)
    {
        try {
            $entityManager = $this->getDoctrine()->getManager();

            $lastposts = $entityManager->getRepository(Post::class)->findBy([], ['id' => 'DESC']);
            $post = $entityManager->getRepository(Post::class)->findBy([], ['id' => 'DESC']);
            $categories = $entityManager->getRepository(Category::class)->findAll();

            if (is_null($post)) {
                throw new \Exception("A post with id:$id does not exist!");
            }
        } catch (\Exception $ex) {
            return $this->render('default/error.html.twig', ['errorMessage' => $ex->getMessage()]);
        }

        return $this->render('default/post.html.twig', ['post' => $post], ['lastposts' => $lastposts]);
    }

    /**
     * @Route("/article/{id}/{name}", name="post-action")
     */
    public function post($id, $name)
    {
        try {
            $entityManager = $this->getDoctrine()->getManager();
            $post = $entityManager->getRepository(Post::class)->find($id);

            if (is_null($post)) {
                throw new \Exception("A post with id:$id does not exist!");
            }
        } catch (\Exception $ex) {
            return $this->render('default/error.html.twig', ['errorMessage' => $ex->getMessage()]);
        }

        return $this->render('default/post.html.twig', ['post' => $post]);
    }

    /**
     * @Route("/article-add", name="article-add-action")
     */
    public function createPost(Request $request)
    {
        $article = new Post();
        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $article = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($article);
            $entityManager->flush();
        }

        return $this->render('admin/addArticle.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/article-edit/{id}", name="article-edit-action")
     */
    public function editArticle(Request $request, $id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $article = $entityManager->getRepository(Post::class)->find($id);
        $form = $this->createForm(ArticleType::class, $article);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $article = $form->getData();

            $entityManager->persist($article);
            $entityManager->flush();
        }

        return $this->render('admin/addArticle.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/about", name="about-action")
     */
    public function about()
    {
        return $this->render('default/about.html.twig');
    }

    /**
     * @Route("/contact", name="contact-action")
     */
    public function contact()
    {
        return $this->render('default/contact.html.twig');
    }

    /**
     * @Route("/portfolio", name="portfolio-action")
     */
    public function portfolio()
    {
        return $this->render('default/portfolio.html.twig');
    }

    /**
     * @Route("/services", name="services-action")
     */
    public function services()
    {
        return $this->render('default/services.html.twig');
    }

    /**
     * @Route("/subscribe", name="subscribe")
     */
    public function subscribe(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $subscriber = new Subscriber();
        $form = $this->createForm(SubscriberType::class, $subscriber);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $subscriber = $form->getData();

            $entityManager->persist($subscriber);
            $entityManager->flush();
        }

        return $this->render('default/subscribe.html.twig', ['form' => $form->createView()]);
    }

    /**
     * @Route("/send-newsletters", name="send-newsletters")
     */
    public function sendNewsletters(Request $request, \Swift_Mailer $mailer)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $subscribers = $entityManager->getRepository(Subscriber::class)->findAll();
        $lastArticle = $entityManager->getRepository(Post::class)->findBy([], ['id' => 'DESC'], '1');
        foreach ($subscribers as $subscriber) {
            $body = "";
            $message = (new \Swift_Message('Newsletter ' . date('Y-m-d')))
                ->setFrom('daniel@ianosi.ro')
                ->setTo($subscriber->getEmail())
                ->setBody($this->renderView('default/newsletter.html.twig', ['post' => $lastArticle[0]]), 'text/html');
            $mailer->send($message);
        }
        return $this->redirectToRoute('homepage');
    }

    public function commentNew(Request $request, Post $post)
    {
        $comment = new Comment();
        $comment->setUsername($this->getUser());
        $post= $comment->getContent();

        $form = $this->createForm(Comment1Type::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($comment);
            $em->flush();
            return $this->redirectToRoute('post-action', ['comment' => $post->getComments()]);


        }
    }
}

